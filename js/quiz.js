//this is a javascript file which will handle the form submission specifically.
//button won't whatever, but a restart button will appear.

function validate_form($form, $inputs, $button)
{
    $button.addClass('btn--inactive');
    cursor_loading(true);

    var url = $form.attr("action");

    var form_string =  $form.serialize();

    $.post(url, form_string, function(response) {

        var form_valid;
        $section_top = $('section section').first();
        $section_middle = $section_top.next();

        if (response.success) {

            form_valid = form_is_indeed_valid($inputs, $button);

        } else {

            cursor_loading(false);
            inputs_readonly(false, $inputs);

            insert_errors_into_form(response.errors);
            $section_middle.addClass('hidden');
            form_valid =  false;
        }

        return form_valid;
    });
}

function form_is_indeed_valid($inputs, $button)
{
    if ($section_middle.hasClass("hidden")) {

        url = $button.data("url");
        inputs_readonly(true, $inputs);

        //load the quiz.  duh.
        $to_hide = $();
        $form_username = $('form.quiz_form input#form_username');
        if(! $form_username.val())
            $to_hide = $form_username.parents('.form_row');

        load_quiz(url, $button, $to_hide);
        //this line allows one to restart, which means--among other things--that the run_quiz()
        //function is run twice, which causes errors to be thrown later on.  This breaks the program.
        //$button.addClass("approved restart").text("Restart?");
        $button.addClass("approved").text("Approved!");


        return true;
    }
    return false;
}

$(document).ready(function() {

    $('section.section__bun').last().addClass('has_results');

    //invalidate the first item on that form
    $('select#form_year option:selected').attr("disabled", "disabled");

    $check_form_button = $('p.button_container span.btn');

    $form_elements_top = $('#form_full_name, #form_year, form.quiz_form input#form_username');
    $form_if_username = $('#form_if_username');
    $form_username = $form_elements_top.last();
    $all_inputs = $form_elements_top.add($form_if_username).add($form_if_username.children());

    /**
     * Code the triggers a button press if the 'enter' key is pressed inside of an input field
     * In this case, either the name, the year, or the username field will cause the event to be triggered.
     */
    $("div.form_row input, div.form_row select").keypress(function(e)
    {
        var code = (e.keyCode ? e.keyCode : e.which);

        //13 is the Enter key.
        if (code == 13)
        {
            $check_form_button.trigger("inputEnterEvent");
            return false;
        }
    });

    $check_form_button
        .on("click", function()
        {
            if($(this).hasClass("restart"))
                restart();

            else
                button_action_on_click_inputEnter($(this), $all_inputs);
        })
        .on("inputEnterEvent", function()
        {
            button_action_on_click_inputEnter($(this), $all_inputs);
        });

    function button_action_on_click_inputEnter($button, $inputs)
    {
        if(! $button.hasClass("btn--inactive"))
        {
            $('p.error').remove();
            //at this point, take the values in the form and try to validate them
            validate_form($('form'), $inputs, $button);
            inputs_readonly(true, $inputs);
        }
    }

    function restart()
    {
        inputs_readonly(false, $form_elements_top.add($form_if_username).add($form_if_username.children()));
        $form_elements_top.val('');
        $form_if_username.removeClass("hidden").find('input[type="checkbox"]').prop("checked", false);
        $check_form_button.removeClass("approved restart");
        if(! $form_elements_top.filter('form.quiz_form #form_username').is(':visible'))
            $form_elements_top.filter('form.quiz_form form_username').parents('.form_row').slideDown(300);

        $form_elements_top.filter('#form_year').addClass('inactive');

        $quiz = $('section.section__burger').add($('section.section__bun:last-of-type'));

        unfold($('section.section__bun:first-of-type'));
        $quiz.addClass('hidden fold');
        $quiz.children().not('.section_title').remove();
        $check_form_button.text("I want to do the quiz!");
        reset_bar_bg();

    }

    $form_elements_top.filter('#form_year').on("focus", function() {
        $(this).removeClass("inactive");
    });


    $form_elements_top.on("change", function( event ) {
        $(this).removeClass("rejected").parents('.form_row').find('p.error').slideUp(120);

        if(! $form_elements_top.hasClass("rejected"))
            $check_form_button.removeClass("btn--inactive");

    });

    $form_if_username.find('input[type="checkbox"]').on("change", function() {
        $(this).parent().addClass('hidden');

        setTimeout(function () {
            $form_username.focus();
        }, 285);
    });

    $form_username.on("focusout", function() {

        if(! $(this).val())
        {
            $form_if_username.find('input[type="checkbox"]').prop("checked", false);
            $form_if_username.removeClass('hidden');
        }
    });
});

/**
 * click event attached to the answers.  Does something when you click them
 *
 $form.children('.form_row').not('.hidden, .disregard').find('input, textarea, select').on('change', function( event ) {

        //if empty, let me know.
        $parent = $(this).parents('.form_row');
        $all_parents = $parent.siblings('.form_row').not('.hidden').add($parent);

        var dirty_inputs = true;

        $all_parents.each(function() {

            if(dirty_inputs)
                dirty_inputs = return_false_if_inputs_empty($(this));
        });

        if(dirty_inputs){
            validate_form($form);
        }
    });

 /*
 function return_false_if_inputs_empty($parent)
 {
     //if empty, let me know.
     $inputs = $parent.find('input, textarea, select');

     var dirty = false;

     //if checkboxes,
     if( $inputs.is('[type="checkbox"]') )
     {
         $inputs.each(function() {

             if(!dirty)
                 dirty = $(this).is(':checked');
         } );
     }
     else
     {
         //verifies inputs, selects, and textareas
         $inputs.each(function () {

             if(!dirty)
                 if($(this).val())
                     dirty = true;
         });
     }

     return dirty;
 }
 */
