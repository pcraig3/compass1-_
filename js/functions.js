/* starts quiz */
function init_quiz(first_question) {

    first_question.addClass("selected");
    reset_bar_bg();
    return first_question;
}

function reset_bar_bg()
{
    progress_bar(0, 1);  //arbitrary value because 0/x = 0;
    increment_background_position(0, 1);
}

/* styles the whole page as 'cursor loading'  A bit hack. */
function cursor_loading(loading)
{
    if(loading)
        $("*").css("cursor", "wait");
    else
        $("*").css("cursor", "");
}

//run this function whenever the answer is clicked.
function progress_bar(numerator, denominator)
{
    $progress = $("div#progress__trough");

    var percent = Math.round((numerator / denominator) * 1000) / 10;

    //console.log("Numerator: " + numerator);
    //console.log("Denominator: " + denominator);
    //console.log("Percent: " + percent);

    $progress.find('#progress__bar').width(percent + "%");

    if(numerator == denominator)
        $progress.find('#progress__bar').addClass('approved');

    else
        $progress.find('#progress__bar').removeClass('approved');

}
/**************************************************************************************/

/* This is a smooth scroll option.
 *  It's stolen from
 *  http://tympanus.net/codrops/2010/06/02/smooth-vertical-or-horizontal-page-scrolling-with-jquery/
 */

/*  Animates the page scrolling to the input element on the page.  Accepts a jquery object. */
function scroll_to($scrollable) {
    $('html, body').stop().animate({
        scrollTop: $($scrollable).offset().top
    }, 700);
}

function unfold($section)
{
    if(! $(this).hasClass('empty')){

        //hide all of the other sections
        $section.siblings().each(function() {
            $(this).addClass("fold").find('.foldable').addClass("hidden");
        });

        //check for visible contents
        var content_size = $section.removeClass("hidden").find('.foldable').size();

        if(content_size)
            $section.removeClass("fold").find('.foldable').removeClass("hidden");
        else
            $section.addClass('empty');

        setTimeout(function () {
            min_window_height();
        }, 520);

    }
}

/************************************************************************************/

/**
 * Keyup was running twice earlier, shipping much more of the quiz than was desirable.  Hopefully this solves it.
 * http://stackoverflow.com/questions/1402698/binding-arrow-keys-in-js-jquery
 */
$(document).keydown(function (e) {

    var keyCode = (e.keyCode ? e.keyCode : e.which),
        arrow = {left: 37, up: 38, right: 39, down: 40, back: 8 };

    switch (keyCode) {
        case arrow.left:
            $('span#back').trigger("leftArrowEvent");

            break;
        case arrow.back:
            $('span#back').trigger("leftArrowEvent");
            if($('.questions').is(':visible'))
                e.preventDefault();
            break;

        case arrow.right:
            $('span#skip').trigger("rightArrowEvent");

            break;

        /*case arrow.up:
         $status.html('up!');
         break;
         case arrow.down:
         $status.html('down!');
         break;
         */
    }
    //e.preventDefault(); // prevent the default action (scroll / move caret)
});

function run_quiz()
{
    var class_advance_quiz = "onward";

    //add some elements to multiple choice answers
    //add a 'select all that you want' span and a 'next answer' span
    $("div.prompt.multiple")
        .append('<span class="answer ' + class_advance_quiz + '" > >> Next Question! </span>')
        .children(".question").after('<span class="subhead">(Select all that apply)</span>');

    $prompt = $("div.prompt");
    $prompt_selected = init_quiz($prompt.first());
    user_path = {};
    last_user = [];
    last_user['full_name'] = "first2013";
    $form = $('form');

    $submit = $("p.button_container input");

    $('.section_title').on('click', function() {
        unfold($(this).parent());
    });


    /**
     * click event attached to the answers.  Does something when you click them
     */
    $prompt.delegate('span.answer', 'click', function( event ) {

        if(!($(this).parent().hasClass('selected'))) {
            //do nothing if an answer is clicked outside of the selected class
        }
        else
        {
            //logic governing skipping back and forth through the quiz
            remove_future_answers();

            //adds the selected answer to the user's profile and potentially advances the quiz.
            $next_question = process_answer($prompt_selected, $(this));

            //console.log(parse_ratings($(this).data('ratings'))); //ratings from the selected answer
            ////*console.log(user_path);  // user's path so far.
            $(this).toggleClass('path');

            if($next_question)
                change_border(user_path);
        }
    });

    /**
     * function that looks for all path elements up ahead and deletes their values from the user's path
     *
     * Use case: user has answered some questions but then comes back and changes his or her answers.
     */
    function remove_future_answers() {

        var $paths = [];

        if($prompt_selected.hasClass("single"))
        {
            //get all answers with the .path class INCLUDING current prompt
            $paths =  $prompt_selected.nextAll('.prompt').add($prompt_selected).children('.path');
        }
        else
        if($prompt_selected.hasClass("multiple"))
        {
            //get all answers further ahead in the quiz
            //ones in this prompt are skipped because question is multiple choice.
            $paths =  $prompt_selected.nextAll('.prompt').children('.path');
        }

        if($paths.length)
        {
            //get all of their ratings and subtract them from the user's profile
            //don't select any of the auto generated 'done' buttons in the multiple choice questions.
            subtract_answers_further_ahead($paths.filter('.answer').not('.' + class_advance_quiz));

            //remove all 'skipped' elements.
            $paths.filter('.skipped').remove();
            //remove all 'path' classes.
            $paths.removeClass('path');
        }
    }

    /**
     * Functions as an if statement, deciding what to do with the answer we have clicked
     * @param $current_prompt the current prompt in which we have clicked an answer
     * @param $clicked_answer which answer specifically we have picked
     */
    function process_answer($current_prompt, $clicked_answer)
    {
        if($current_prompt.hasClass("single"))
            return process_single_answer($clicked_answer);
        if($current_prompt.hasClass("multiple"))
            return process_multiple_answer($clicked_answer);

        return false;
    }

    /**
     * Processes an answer to a question which accepts one answer.  The
     * @param $clicked_answer
     * @returns true so as to advance the quiz
     */
    function process_single_answer($clicked_answer)
    {
        //add to the user's path the ratings attached to this qa_pair
        flagstone(user_path, parse_ratings($clicked_answer.data('ratings')));

        return true;
    }

    /**
     * Use cases:
     *      clicked answer becomes .path, doesn't advance the quiz.
     *      clicked answer removes .path, doesn't advance the quiz
     *      clicked answer advances the quiz.
     */
    function process_multiple_answer($clicked_answer)
    {
        //clicked answer advances the quiz
        //doesn't have any useful metadata
        if($clicked_answer.hasClass(class_advance_quiz))
            return true;

        //remove the 'path' class from the span that advances the quiz.
        $clicked_answer.siblings("." + class_advance_quiz).removeClass("path");

        //clicked answer is one we already clicked
        //subtract all of its attributes
        if($clicked_answer.hasClass("path"))
        {
            subtract_answers_further_ahead($clicked_answer);
        }
        //clicked answer's attributes should be added, but without advancing the quiz
        else
        {
            //what do do with a normal answer
            flagstone(user_path, parse_ratings($clicked_answer.data('ratings')));
        }
        //stops the quiz from advancing
        return false;
    }

    ///SKIP button
    $('span#skip').on("click rightArrowEvent rightSwipeEvent", function() {
        if($prompt.hasClass('selected')) {

            //add a path element to the final answer of multiple choice questions
            if($prompt_selected.hasClass("multiple"))
                $prompt_selected.children("." + class_advance_quiz).addClass("path");

            //if the prompt has no path elements
            if($prompt_selected.children(".path").size() == 0)
            {
                //add a 'skipped' div
                $prompt_selected.append(
                    '<span class="skipped path"></span>'
                );
            }
            change_border(user_path);
        }

    });

    ///BACK button
    $('span#back').on("click leftArrowEvent leftSwipeEvent", function() {
        if($prompt.hasClass('selected')) {
            back_border($prompt_selected);
        }

    });

    /* Takes a string formatted like "caffeinated,-1;healthy,1;joe_sixpack,-1;"
     Returns an object of keys and values: "{caffeinated: -1, healthy: 1, joe_sixpack: -1}"
     */
    function parse_ratings(ratings)
    {
        result = {};

        arr_1 =  ratings.split(";");

        for(var index = 0; index < (arr_1.length)-1; index++)
        {
            temp = arr_1[index].split(",");

            //at this point, temp is an array with two values.
            result[temp[0]] = parseInt(temp[1], 10);
        }

        return result;
    }

    /* If the user's path contains the key/value pair, add the integer value to the user's current integer value
     else add in a new key/value pair to the user's path
     Note: Accepts an object like {coffee: 1, tea: 0, orange: -3}
     */
    function flagstone(u_path, ratings)
    {
        for (var attribute in ratings) {
            if(attribute in u_path)
            {
                u_path[attribute] += ratings[attribute];
            }
            else
                u_path[attribute] = ratings[attribute];
        }
        return u_path;
    }

    //get all of their ratings and subtract them from the user's profile
    function subtract_answers_further_ahead($answers_to_subtract)
    {
        //for each answer, get its rating and then remove it.

        $answers_to_subtract.each(function() {
            // Returns an object of keys and values: "{caffeinated: -1, healthy: 1, joe_sixpack: -1}"
            ratings = parse_ratings($(this).data('ratings'));

            //for each attribute, subtract it from the user's path and delete it if its value is zero
            for (var attribute in ratings) {
                if(attribute in user_path){
                    user_path[attribute] -= ratings[attribute];
                    if(! user_path[attribute])
                        delete user_path[attribute];
                }
            }
        });
    }

    /**
     * function moves the quiz forward.
     * Moves the styled border down the quiz (by removing and adding the 'selected' class)
     */
    function change_border(u_path)
    {
        progress_bar($prompt_selected.children('.question').attr('id'),
            $prompt.last().children('.question').attr('id'));

        //this should append the prompt to the left grid.
        $prompt_selected.removeClass("selected").addClass("passed_over");

        //if prompt.selected is not last, keep going.
        if(!($prompt_selected.is($prompt.last()))) {

            //automatically add border to next question
            $prompt_selected = $prompt_selected.next();
            $prompt_selected.addClass("selected");

            increment_background_position($prompt_selected.children('.question').attr('id'), percent_forward);



            /*
             * This is recursive.
             * After changing the border, we make sure that the current question we want to ask is okay to ask.
             * if not, we run the method again.
             */
            selected_prerequisites = parse_ratings($prompt_selected.find('span.question').data('prerequisites'));

            /* if the prerequisities don't add up, try the next question */
            if(!check_prerequisites(u_path, selected_prerequisites)) {
                change_border(u_path);
            }
        }
        //else, we've finished the quiz
        else
        {
            $('span#back').add($('span#skip')).addClass('disabled');

            //if the '.auto_submit_false' class is found, then don't automatically submit.
            if($('.auto_submit_false').size())
            {
                unfold($('section.section__bun').last());
                $submit.slideDown(200);
                //scroll_to($submit);

                //use JS to fill up the path variable.  String is returned to Controller when form is submitted.
                $('input#form_path').val(JSON.stringify(user_path));
                $('input#form_answers').val(JSON.stringify(return_answer_ids_as_array()));
            }
            else
            {
                cursor_loading(true);
                //insert the current user's data into the database.
                $('section.section__burger').slideUp(300);
                last_user = insert_user(u_path);
            }
        }
    }

    function return_answer_ids_as_array()
    {
        var arr = [];

        $('span.answer.path[id]').each(function() {
            arr.push($(this).attr("id"));
        });

        return arr;
    }

    /* Check how the prerequisites for the selected question match against the user's path
     Ask a question if :
     -A prereq isn't in the user's path (they have never been asked about it)
     -A prereq has been met by the user (they have shown interest in at least one area)
     */
    function check_prerequisites(u_path, selected_prerequisites)
    {

        // we want it to work thus:
        // it will cycle through all of them, and if it finds any that pass, we ask this question.
        for (var prereq in selected_prerequisites) {

            //if the prerequisite for the question is in the user's current path
            if(prereq in u_path)
            {
                //if the value in the user's the current path is equal or greater
                // than the value of the current question, return true (ask the question)
                if(u_path[prereq] >= selected_prerequisites[prereq])
                {
                    /* console.log("user path: " + u_path[prereq]); */
                    /* console.log("question: " + selected_prerequisites[prereq]); */
                    return true;
                }

            }
            //if the prerequisite is not found in the user's profile and value is zero,
            // return true (ask the question).
            //if its value is higher, skip the question
            else{
                if(selected_prerequisites[prereq] < 1)
                    return true;
                else
                    return false;
            }

        }
        //if we have cycled through all of the prerequisites and found none that aren't in the user's path
        // and none that aren't fulfilled, return false (don't ask the question)
        return false;
    }

    /*  recursive function that goes backwards looking for a prompt containing a .path element
     .path elements are applied to answered questions and skipped questions
     function stops when it gets to the first question */
    function back_border($cur_prompt) {

        //if(--times_incremented < 0)
        //  times_incremented = 0;

        //if current prompt is not first prompt
        if(! $cur_prompt.is($prompt.first()) )
        {
            $prev_question = $cur_prompt.prev();

            //if the previous div is a prompt (ie, you can't go backwards from the first question)
            if($prev_question.hasClass("prompt"))
            {
                $prev_question.removeClass("passed_over");
                //if the prompt has been answered or skipped, its children will have a path class
                if($prev_question.children().hasClass("path"))
                {
                    $prompt_selected.removeClass("selected");
                    $prompt_selected = $prev_question;
                    $prompt_selected.addClass("selected");

                    progress_bar($prompt_selected.children('.question').attr('id'),
                        $prompt.last().children('.question').attr('id'));

                    increment_background_position($prompt_selected.children('.question').attr('id'), percent_forward);


                }
                else {
                    //console.log("You haven't answered the question");

                    back_border($prev_question);
                }
            }
        }
        //else
        //console.log("You can't go backwards from the first question");
    }

    function insert_user(u_path)
    {

        $('section.section__bun').first().children('h2').text("Loading...");

        $full_name = $('input#form_full_name');
        var full_name = sanitize_form_input($full_name.val(), "full_name");
        $year = $('select#form_year');
        var year = sanitize_form_input($year.val(), "year");

        $username = $('form.quiz_form input#form_username');

        var username = "";
        if($username.val())
            username = sanitize_form_input($username.val(), "username");

        //for each entry in the path, we have to create a rating entry.
        //a user in the database is made up of a name, a potential username, a year,
        // an ip address, the user's path (ratings
        //gotten from qa_pairs), and a(n automatic) timestamp.
        var path_as_string = JSON.stringify(u_path);

        //record each answer that the user picked.
        var answers_as_string = JSON.stringify(return_answer_ids_as_array());

        //get the user_url
        insert_user_url = $form.data("url");

        $.post(insert_user_url, {  full_name: full_name, year: year, username: username,
            profile: path_as_string, answers: answers_as_string })
            .done(function(data){
                ////*console.log(JSON.stringify(data));

                $full_name.val(data.full_name);
                $year.val(data.year);
                $username.val(data.username);

                //CALL THE RESULTS
                return_results($('div#results').data("url"));
            });


        var last_user = [];
        last_user['full_name'] = full_name;

        return last_user;
    }

    function sanitize_form_input(input, prepend_string)
    {
        prepend_string = prepend_string || "input";

        //http://css-tricks.com/snippets/javascript/strip-html-tags-in-javascript/
        input = input.replace(/[^\w\s]/gi, '');

        //returns true if the replace call returns zero characters
        if(!(input.length))
        {
            //return a random number between 1 and 10,000
            input = prepend_string + (Math.floor((Math.random()*10000)+1));
        }

        return input;
    }



    //RESTART button
    /* restart the quiz:
     clear all user_path data, return 'selected' class to first question, hide buttons */
    $("span.orange").click(function () {
        $prompt.removeClass('passed_over').children('span.answer').removeClass('path');
        $prompt_selected = init_quiz($prompt.first());
        user_path = {};
        $("div#results").html("");
        $('section.section__burger').slideDown(300);

        var enable = {
            backgroundColor : "#ffffff",
            color: "#000000",
            cursor: ""
        };
        //money
        $input.prop('disabled', false).css(enable).val("");
    });

    //RESULTS button
    /*  return the results:
     send the first and last name to the database, return results based on user's ratings, build html for results,
     hide the results button so the user can't keep clicking it, indicate when the cursor is/isn't loading *
     $("span.blue").click(function () {

     cursor_loading(true);

     //return all opportunities//
     results_url = $(this).data("url");
     */

    function return_results(results_url)
    {
        $.post(results_url, { full_name: last_user['full_name'] })
            .done(function(data){
                //console.log(data);

                parse_and_append_results(data);
                //shoot up the results
            });
    }

    Hammer($('div.questions')).on("swiperight", function(ev) {
        $('span#back').trigger("leftSwipeEvent");
        ev.gesture.stopDetect();
        ev.gesture.preventDefault();
    })
        .on("swipeleft", function(ev) {
            $('span#skip').trigger("rightSwipeEvent");
            ev.gesture.stopDetect();
            ev.gesture.preventDefault();

        });


}

/* Opportunites have been rated by this point, but not in any order.
 Return those with the highest values out of the returned set of opportunities
 */
function get_max_results(opportunity_arr, results, total)
{

    //if the length of the results array is greater than or equal to the 'total' array, return results
    if(total - results.length <= 0 || counter() > total)
    {
        counter(false);
        return results;
    }

    //if not, find the maximum value currently in the opportunity_arr array and put all opportunities
    //with that value into the 'results' array.  Delete them from the opportunity_arr array.

    var max = 0;

    //after this, we should have the max value
    for(var index in opportunity_arr) {
        if (opportunity_arr.hasOwnProperty(index)) {

            opp = opportunity_arr[index];

            if(opp.hasOwnProperty('after_category_score'))
                if(opp.after_category_score > max)
                    max = opp.after_category_score;
        }
    }

    //put all with max value into 'results'
    for(index in opportunity_arr) {
        if (opportunity_arr.hasOwnProperty(index)) {

            if(opportunity_arr[index]['after_category_score'] == max) {
                results[results.length] = opportunity_arr[index];
                delete opportunity_arr[index];
            }
        }
    }

    return get_max_results(opportunity_arr, results, total);
}

/* http://stackoverflow.com/questions/1535631/static-variables-in-javascript */
var counter = (function() {
    var count = 1; // This is the private persistent value
    // The outer function returns a nested function that has access
    // to the persistent value.  It is this nested function we're storing
    // in the variable counter above.
    return function(reset) {
        if(reset == false) //if a zero or false is passed into counter(), then id is reset
            count = 0;

        return count++;
    };  // Return and increment
})(); // Invoke the outer function after defining it.

function parse_and_append_results(data)
{
    //turn string into a JS object.
    data = JSON.parse(data);

    total = 50;

    //call recursive method which returns only the 'winners' array
    results = get_max_results(data, results, total);

    count = 1;


    //fix the photo in place.
    $('div#bg').css("backgroundAttachment", "fixed");



    $results = $('div#results');
    $results.parent().hide();

    for(var i=0; i < 12; i++) {

        $cycle_result_columns().append(make_new_result(results));

        count++;
    }

    cursor_loading(false);
    //unfold($('section.section__bun').last());

    //remove the top element entirely
    $first_bun = $('section.section__bun').first().slideUp(120);

    unfold($results.parent());
    $results.parent().fadeIn(1500).addClass("reveal");

    setTimeout(function () {
        $first_bun.remove();
    }, 150);
}

var results = [];

function make_new_result(results)
{
    opp = results.shift();

    return ('<div class="result category_' + opp.category_id + '">'
        + '<a target="_blank" href="https://' + opp.link + '">' + '<h3 class=\"result_title\" title="Check them out on WesternLink!" id="' + opp.after_category_score + '"> ' + opp.name + ' </h3></a>'
        + '<a target="_blank" href="' + return_category_link(opp.category_name.split(" ")[0]) + '" ><h4 class="category_' + opp.category_id + '" title="See more from this category!">' + opp.category_name + '</h4></a>'
        + '<p class="summary">' + opp.summary + '</p>'
        + '<div class="collapse">'
        + '<span id="' + opp.id + '" class="helpful">I found this helpful</span><span id="' + opp.id + '" class="not_helpful">Not what I wanted</span>'
        + '</div>'
        + '</div>');
}

/**
 * This idea behind this function is that we store the columns in a jquery object and
 * then we return a new one every time.
 *
 var $cycle_result_columns = (function() {

    var $result_columns = $result_columns || $('#results').find('div.grid__item');
    var count = count || 0;

    return function()
    {
        modulo = count % 3;

        return $($result_columns.get(modulo));
    };
})();*/

/**
 * This idea behind this function is that we store the columns in a jquery object and
 * then we return a new one every time.
 */
$cycle_result_columns = (function() {

    var $result_columns = $result_columns || $('#results').find('div.grid__item');
    var col = 0;

    return function() {

        modulo = col++ % 3;

        return $($result_columns.get(modulo));
    }
})();

$('div#results')
    //adds functionality to the 'feedback' buttons in result
    //clicking 'helpful' or 'not helpful' silently updates opportunity in the database
    .delegate('div.collapse span', 'click', function() {

        //get the result_div before the span is removed
        $result_div = $(this).parents('div.result');

        var whether_helpful = $(this).attr("class");

        feedback_url = $('div#results').data("feedback");

        $.post(feedback_url, { whether_helpful: whether_helpful, id: $(this).attr('id') })
            .done(function(data){
                //console.log(data);
            });

        //sexy programming replaces feedback button with slowly a slowly disappearing thank you method
        $collapse = $(this).parents().first();
        $(this).add($(this).siblings('span')).remove();
        //so baller
        $collapse.append('<span class="feedback">Thanks for your feedback!</span>').find('span').delay(1000).slideUp(500);

        if(whether_helpful == "not_helpful")
        {
            $result_div.delay(1000).fadeOut(300);

            setTimeout(function () {
                if(results.length)
                {
                    $new_result = make_new_result(results);
                    $result_div.replaceWith($new_result);
                    $($new_result).hide().fadeIn(300);
                }
                else
                    $result_div.remove();

            }, 1350);
        }
    });

function return_category_link(first_word)
{
    arr = [];
    arr["Academics"] = "https://westernu.collegiatelink.net/organizations?SearchValue=Academics&SearchType=Category&CurrentPage=1&SelectedCategoryId=156";
    arr["Business"] = "https://westernu.collegiatelink.net/organizations?SearchValue=Business%20and%20Finance&SearchType=Category&CurrentPage=1&SelectedCategoryId=157";
    arr["Community"] = "https://westernu.collegiatelink.net/organizations?SearchValue=Community%20Volunteering%2F%20Philanthropy&SearchType=Category&CurrentPage=1&SelectedCategoryId=161";
    arr["Cultural"] = "https://westernu.collegiatelink.net/organizations?SearchValue=Cultural&SearchType=Category&CurrentPage=1&SelectedCategoryId=169";
    arr["Health"] = "https://westernu.collegiatelink.net/organizations?SearchValue=Health%20and%20Wellness&SearchType=Category&CurrentPage=1&SelectedCategoryId=160";
    arr["Hobbies/"] = "https://westernu.collegiatelink.net/organizations?SearchValue=Hobbies%2F%20Special%20Interests&SearchType=Category&CurrentPage=1&SelectedCategoryId=164";
    arr["International"] = "https://westernu.collegiatelink.net/organizations?SearchValue=International%20Volunteering%2F%20Philanthropy&SearchType=Category&CurrentPage=1&SelectedCategoryId=162";
    arr["Media"] = "https://westernu.collegiatelink.net/organizations?SearchValue=Media%20Production%20&SearchType=Category&CurrentPage=1&SelectedCategoryId=159";
    arr["Music"] = "https://westernu.collegiatelink.net/organizations?SearchValue=Music%20and%20Performing%20Arts&SearchType=Category&CurrentPage=1&SelectedCategoryId=165";
    arr["Politics/"] = "https://westernu.collegiatelink.net/organizations?SearchValue=Politics%2F%20Social%20Justice&SearchType=Category&CurrentPage=1&SelectedCategoryId=163";
    arr["Sports"] = "https://westernu.collegiatelink.net/organizations?SearchValue=Sports&SearchType=Category&CurrentPage=1&SelectedCategoryId=166";

    return arr[first_word];
}

jQuery.fn.visible = function(num) {
    return this.animate({visibility: "visible"}, num);
};

jQuery.fn.invisible = function() {
    return this.animate({visibility: "visible"}, 300);
};

jQuery.fn.visibilityToggle = function() {
    return this.css('visibility', function(i, visibility) {
        return (visibility == 'visible') ? 'hidden' : 'visible';
    });
};

/****************** Controls the bg image as well as hidden pop-up menu *********************************************/

$div_bg = $('div#bg');
var bg_width = 2335;
var bg_height = 505;
var times_incremented = 0;
var percent_forward = 0.9;

$(window).resize(function(event) {

    var temp = bg_height;
    set_bg_width_height();

    if(temp !== bg_height)
    {
        //adjust the pixels
        increment_background_position(times_incremented, percent_forward);

    }

    //set the minimum height of the questions div so that it fills up the screen.
    min_window_height();
});

$under = $('div#under');

$('img#logo, div#wrapper').click(function() {

    if($(this).hasClass("push_down"))
    {
        $('div#wrapper').toggleClass("push_down");

        $under.slideToggle(300);
        $under.find('div.sneaky_content').fadeOut(300);
    }

});

$under.on("click", function() {
    $under.slideUp(200);
    $('div#wrapper').removeClass("push_down");
});

$under.find('div#sneaky_menu, div.sneaky_content').on("click", function(e){
    e.stopPropagation();
});


function increment_background_position(times_incremented, offset)
{
    var back_position = $div_bg.css("backgroundPosition");
    var percent = back_position.indexOf('px');

    //cuts off everything before the first percent sign;
    var leftmost = two_decimal_places(back_position.substring(0, percent));
    var rightmost = back_position.substring(percent); //cuts off everything after and including the first percent

    rightmost = "px 40%";  //because IE and Chrome apparently work differently

    leftmost = convert_percentage_to_pixels(times_incremented, offset);

    if(leftmost > bg_width)
        leftmost = leftmost - bg_width;

    //console.log(leftmost + rightmost);

    $div_bg.css("backgroundPosition", "-" + leftmost + rightmost);
}

/*
 http://stackoverflow.com/questions/5106243/how-do-i-get-background-image-size-in-jquery
 doesn't seem to work properly in IE.  Not a fatal flaw.
 */
function get_h_w()
{
    var img = new Image;
    if($(window).width() > 480)
        img.src = $('div#bg').css('backgroundImage').replace(/url\(|\)$/ig, "");

    else
        img.width = 0;

    return img;
}

function convert_percentage_to_pixels(times_incremented, denominator)
{
    //figure out the units required.
    if(times_incremented < 1)
        return 0;

    var pix = two_decimal_places((bg_width * denominator) / 100);

    //console.log("pix: " + pix + "; bg_width: " + bg_width);
    return pix * times_incremented;
}


function two_decimal_places(number)
{
    return parseFloat(parseFloat(number).toFixed(2));
}

function set_bg_width_height()
{
    bg_img = get_h_w();

    if(bg_img.width != 0){
        bg_width = bg_img.width;
        bg_height = bg_img.height;
    }

}

function check_full_height()
{
    var window_h = $(window).height();
    var wrapper_h = $('div#master').height();

    if(wrapper_h < window_h)
        $('html').add('body').add('div#master').css("height", "100%");
}
/******************************************************************************************/

function min_window_height()
{
    $grid_items = $('.questions').find('.grid__item');

    if($('div.prompt').not('.passed_over').size())
    {

        if($grid_items.length > 0 &&
            ! $grid_items.parents('section.section__burger').hasClass("fold"))
        {
            var html_height = $('html').height();
            var questions_offset = $grid_items.offset().top;

            var questions_min_height = (html_height - questions_offset -1) + "px";

            //the -1 accommodates for the border on the bottom.
            $grid_items.css("minHeight", questions_min_height);
        }
    }
    else
        $grid_items.css("minHeight", 0);
}

var fadeOut = 300;
var fadeIn = 400;

$('a#about_link').click(function(e) {
    //hide all of the menus
    $('div.sneaky_content#feedback').fadeOut(fadeOut);

    setTimeout(function () {
        $('div.sneaky_content#about').fadeIn(fadeIn);
    }, fadeOut);

    e.preventDefault();
});

$('a#feedback_link').click(function(e) {
    //hide all of the menus
    $('div.sneaky_content#about').fadeOut(fadeOut);

    setTimeout(function () {
        $('div.sneaky_content#feedback').fadeIn(fadeIn);
    }, fadeOut);

    e.preventDefault();
});

/******************************************************************************************/

$( document ).ready(function() {
    unfold($('section section').first()); //unfold the form part of the quiz.

    check_full_height();

    //eventually, we should get rid of this entirely.
    //run_quiz();


});

$(window).load(function() {

    set_bg_width_height();
});