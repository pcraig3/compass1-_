//why don't we just do it all with javascript?
//send in the array of things into the hidden field
//{{ render(controller('EventBundle:Event:_upcomingEvents')) }}

$(document).ready(function() {
    // Handler for .ready() called.

    //random placeholder value, something we are pretty sure won't ever be replicated.
    var approved_name = return_random_string();

    var $form_password = $('input#form_password');
    var $form_name = $('input#form_name');
    var $form_elements = $form_password.add($form_name);

    //this returns two buttons
    var $buttons = $("p.button_container span");

    var $button = $($buttons.get(0));
    var $quiz_button = $($buttons.get(1));
    var $restart = $($buttons.get(2));

    /*
     The idea here is that if you change the name of the club after you've been approved, you have to re-submit.
     */
    $form_name.on('focusout', function(){
        if($form_name.val() != approved_name)
            trigger_disapproved_name_no_message();
    });

    /**
     * Code the triggers a button press if the 'enter' key is pressed inside of an input field
     * In this case, either the name or the password field will cause the event to be triggered.
     */
    $("div.form_row input").keypress(function(e)
    {
        var code = (e.keyCode ? e.keyCode : e.which);

        //13 is the Enter key.
        if (code == 13)
        {
            $button.trigger("inputEnterEvent");
            return false;
        }
    });

    /**
     * Monster function containing the logic for the Ajax calls leading up to revealing the quiz.
     * *    First, look for an opportunity with the input name
     * *    Second, verify that the password is the same
     * *    Finally, our button allows the user to restart
     * * *      Restarting resets the values so the user has to pick a new name and password.
     */
    $button.on("click inputEnterEvent", function() {

        //if the button is not inactive, do something
        if(! $button.hasClass('btn--inactive'))
        {
            var name = $form_name.val();
            var password = $form_password.val();
            var url;

            //restarts the selection process.  The approved name and password will have to be resubmitted
            if($button.hasClass("password"))
            {
                //if the password bar is 'approved' this means that it is locked.
                if($form_password.hasClass('approved'))
                {
                    //call a method to unlock the bar and change the button text
                    allow_edit_password();
                }
                else
                {
                    cursor_loading(true);
                    $button.addClass('btn--inactive').text("Attempting to update Password...");

                    url = $(this).data("url");
                    //update the database.  if it works, then you can return to where we were before
                    //else, print out an error message.
                    update_password_in_database(name, password, url);

                }
            }
            else

            //if the value does not match the one that was approved AND it is not empty
            if(name != approved_name && name != '')
            {
                cursor_loading(true);
                $button.addClass('btn--inactive').text("Searching...");

                //Get the URL encoded in the form by the twig template.
                url =  $form_name.data("url");

                find_name_in_database(name, url);
            }
            else
            //if the password field is not empty
            if(password != '')
            {
                cursor_loading(true);
                $button.addClass('btn--inactive').text("Verifying Password...");

                //Get the URL encoded in the form by the twig template.
                url =  $form_password.data("url");

                check_password_in_database(name, password, url);
            }


        }
        return false;
    });

    $quiz_button.on("click", function() {

        if($(this).is(':visible'))
        {
            //if the button is not inactive, do something
            if(! $button.hasClass('btn--inactive'))
            {
                //run the quiz.



                cursor_loading(true);
                $buttons.addClass('btn--inactive');

                url = $(this).data("url");

                //load the quiz.  duh.
                load_quiz(url, $buttons, $quiz_button);
            }
        }

    });

    $restart.on("click", function() {

        if($(this).is(':visible'))
        {
            //if the button is not inactive, do something
            if(! $button.hasClass('btn--inactive'))
            {
                restart();
            }
        }

    });

    /**
     * Clear the inputs, set them as editable, remove the 'restart' class from the button so as to restore original
     * functionality, and hide the password field
     */
     function restart()
     {
         inputs_readonly(false, $form_elements);
         $form_name.add($form_password).val('');
         trigger_disapproved_name_no_message(false);
         $quiz_button.add($restart).slideUp(120);
         ////*$button.removeClass('approved password').text('Search for a club').parent().after('<br>');
         $button.removeClass('approved password').text('Search for a club');
         reset_bar_bg();
         
         $quiz = $('section.section__burger').add($('section.section__bun:last-of-type'));
         $quiz.slideUp(150);

         setTimeout(function () {
             $quiz.empty().addClass('hidden fold');
         }, 200);
     }


    /**
     * Post function checks the database for the value, returning a boolean value and, if found.
     * the name of the value contained in the database.  Returning the name of the Opportunity is important because
     * the submitted value is matched irrespective of letter casing.  Whatever string was used to match the Opportunity,
     * the returned name will correspond exactly to the name stored in the database.
     *
     * @param value     the name of the Opportunity we want to check for
     * @param url       the route to post to.  Calls a method in the AjaxFindController.
     */
    function find_name_in_database(value, url)
    {
        $.post(url, {  name: value })
            .done(function(data) {

                if(data.found === 1){
                    trigger_approved_name(data.name);
                }
                else{
                    trigger_disapproved_name(data.found);
                }

                cursor_loading(false);
            });
    }

    /**
     * Post function checks the database that the password matches the value, returning a boolean value if found.
     *
     * @param value     the name of the Opportunity we have previously found
     * @param password  the password of the Opportunity we need to verify
     * @param url       the route to post to.  Calls a method in the AjaxFindController.
     */
    function check_password_in_database(value, password, url)
    {
        $.post(url, { name: value, password: password })
            .done(function(data) {

                if(data.match)
                {
                    //at this point, we want two buttons beside each other
                    trigger_approved_password("Approved!");

                    //load_quiz($button.data("url"));
                }
                else
                    trigger_disapproved_password();

                cursor_loading(false);
            });
    }

    function reset_password_approved_string()
    {
        $form_password.val('').parent().slideUp(200);
        approved_name = return_random_string();
    }

    /**
     * If the submitted name calls zero or multiple Opportunities, inform the user whether their
     * query was inaccurate or too vague.  Afterwards, reset any values that may have been changed.
     */
    function trigger_disapproved_name(amount)
    {
        reset_password_approved_string();
        if(!amount)
            $button.text("Sorry, couldn't find any clubs").removeClass('btn--inactive');
        else
            $button.text("Oops, " + amount + " clubs returned. Please be more specific.").removeClass('btn--inactive');

        setTimeout(function () {
            $button.text("Search for a club");
        }, 2000);
    }

    /**
     * If the submitted name is not a valid Opportunity, inform the user and
     * reset any values that may have been changed.
     */
    function trigger_disapproved_name_no_message()
    {
        reset_password_approved_string();
        $button.text("Search for a club").removeClass('btn--inactive');
    }

    /**
     * If the submitted name is a valid Opportunity, inform the user, store the name in a variable, and reveal
     * the password field
     *
     * @param $name     The name of the Opportunity returned by 'find_name_in_database'
     */
    function trigger_approved_name($name)
    {
        $form_password.parent().slideDown(300);
        $form_name.val($name);
        $button.text("Great, found it!").removeClass('btn--inactive');
        approved_name = $form_name.val();

        setTimeout(function () {
            $button.text("Enter password");
        }, 1200);
    }

    /**
     * If the submitted password is incorrect, inform the user and
     * allow him or her to resubmit.
     */
    function trigger_disapproved_password()
    {
        $form_password.val('');
        $button.text("Sorry, password not recognized.").removeClass('btn--inactive');

        setTimeout(function () {
            $button.text("Enter Password");
        }, 1200);
    }

    function trigger_approved_password(button_text)
    {
        $button.text(button_text).removeClass('btn--inactive').addClass("approved password");
        inputs_readonly(true, $form_elements);

        setTimeout(function () {
            //slide down the new buttons
            $restart.slideDown(200);

            if($('section.section__burger').hasClass('hidden')){
                $quiz_button.slideDown(200);
            }
        }, 1000);

        setTimeout(function () {
            $button.text("Change your password");
        }, 1200);
    }

    function trigger_disapproved_password_update(old_password)
    {
        trigger_approved_password("Failed. Reverting to last password.");

        setTimeout(function () {
            $form_password.val(old_password);
        }, 1000);
    }

    function allow_edit_password()
    {
        //input readable
        inputs_readonly(false, $form_password);

        //change the text on the button
        $button.text("Submit your new password");

        //hide the other buttons
        $quiz_button.add($restart).slideUp(120);
    }

    /**
     * Post function tries to update the value in the database with a new password,
     * returning a boolean value if successful.
     * Empty passwords are not permitted, but everything else is game.
     *
     * @param value     the name of the Opportunity we have previously found
     * @param password  the password of the Opportunity we are trying to update
     * @param url       the route to post to.  Calls a method in the AjaxFindController.
     */
    function update_password_in_database(value, password, url)
    {
        $.post(url, { name: value, password: password })
            .done(function(data) {

                if(data.success)
                {
                    //update the database.  if it works, then you can return to where we were before
                    //else, print out an error message.
                    if(data.match)
                        trigger_approved_password("New password matches old password.");
                    else
                        trigger_approved_password("Success! Password updated!");
                }
                else
                {
                    trigger_disapproved_password_update(data.password);
                }

                cursor_loading(false);
            });
    }

    /**
     * Since the name of a matched Opportunity will be stored in a variable, it is important to give it a
     * randomised value that won't hardly never conflict with the name of any Opportunities.
     */
    function return_random_string()
    {
        return "placeholder_" + Math.floor((Math.random()*10000)+1);
    }

});

/*
 $('input').bind('input keyup', function(){
 var $this = $(this);
 var delay = 2000; // 2 seconds delay after last input

 clearTimeout($this.data('timer'));
 $this.data('timer', setTimeout(function(){
 $this.removeData('timer');

 // Do your stuff after 2 seconds of last user input
 }, delay));
 });
 */