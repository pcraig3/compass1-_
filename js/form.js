
function load_quiz(url, $buttons_active, $buttons_hide)
{
    $.post(url)
        .done(function(data) {
            $section_middle = $('section.section__burger');
            $section_middle.addClass('hidden fold').append(data);
            $buttons_active.each(function(){
                /*$(this).removeClass('btn--inactive');*/
            });

            $buttons_hide.each(function(){
                $(this).slideUp(200);
            });

            cursor_loading(false);
            run_quiz();
            unfold($section_middle);

        });
}

function insert_errors_into_form(errors)
{
    //foreach key in the array, break the message into parts based on periods.
    //$('<p class="error">' + '.</p>').insertBefore('#form' + key).hide().slideDown(300);
    var error_string = "";

    for (var key in errors) {
        if (errors.hasOwnProperty(key)) {
            error_string = errors[key];

            $form_element = $('#form_' + key).addClass("rejected");

            var error_array = error_string.split(".");

            for(var error_message in error_array) {
                if (error_array.hasOwnProperty(error_message)) {

                    if(error_array[error_message])
                        $('<p class="error">' + $.trim(error_array[error_message]) + '.</p>').insertBefore($form_element).hide().slideDown(300);
                }
            }
        }
    }
}

/**
 * Set the input fields as editable or readonly, and then apply or remove styling to indicate their status
 * to the user.
 *
 * @param $readonly         true if inputs are to become readonly.  false if editable.
 * @param $form_elements    a JQuery object with the elements to render (un)editable
 */
function inputs_readonly($readonly, $form_elements)
{
    $form_elements.each(function() {

        $(this).prop("readonly", $readonly);

        if($readonly)
            $(this).addClass("approved");
        else
            $(this).removeClass("approved");
    });
}

function inputs_disabled($readonly, $form_elements)
{
    $form_elements.each(function() {

        $(this).prop("disabled", $readonly);

        if($readonly)
            $(this).addClass("approved");
        else
            $(this).removeClass("approved");
    });
}