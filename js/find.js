
$(document).ready(function() {

    $('section.section__bun').last().addClass('has_results');

    $submit_username_button = $('p.button_container span.btn');

    $form_elements_top = $('form.find_form #form_username');

    $submit_username_button
        .on("click inputEnterEvent", function()
        {
           button_action_on_click_inputEnter($(this), $form_elements_top);
        });

    function button_action_on_click_inputEnter($button, $input)
    {
        if(! $button.hasClass("btn--inactive"))
        {
            $('p.error').remove();
            //at this point, take the values in the form and try to validate them
            validate_form($('form'), $input, $button);
            inputs_readonly(true, $input);
        }
    }

    $form_elements_top.on("focus", function( event ) {
        $(this).removeClass("rejected").parents('.form_row').find('p.error').slideUp(120);

        if(! $form_elements_top.hasClass("rejected"))
            $submit_username_button.removeClass("btn--inactive");

    });
});

//this is a javascript file which will handle the form submission specifically.
//button won't whatever, but a restart button will appear.

function validate_form($form, $input, $button)
{
    $button.addClass('btn--inactive');
    cursor_loading(true);

    find_username_in_database($form.attr("action"), $input.val());
}


/**
 * Post function checks the database for the username, returning a boolean value and, if found.
 * the username contained in the database.  Returning the username of the user is probably not very important, but we did
 * it earlier, so let's just do it again.
 *
 * @param username     the username we want to check for
 * @param url       the route to post to.  Calls a method in the AjaxQuizController.
 */
function find_username_in_database(url, username)
{
    $.post(url, {  username: username })
        .done(function(response) {

            if(response.found === true)
            {

                return_saved_results($('div#results').data("url"), username);
            }
            else
            {

                insert_errors_into_form(response.errors);

                cursor_loading(false);
                inputs_readonly(false, $('form.find_form #form_username'));
            }

            cursor_loading(false);
            return response.found;
        });
}

/**
 * Code the triggers a button press if the 'enter' key is pressed inside of an input field
 * In this case, either the name, the year, or the username field will cause the event to be triggered.
 */
$("div.form_row input, div.form_row select").keypress(function(e)
{
    var code = (e.keyCode ? e.keyCode : e.which);

    //13 is the Enter key.
    if (code == 13)
    {
        $submit_username_button.trigger("inputEnterEvent");
        return false;
    }
});

function return_saved_results(saved_results_url, username)
{
    $.post(saved_results_url, { username: username })
        .done(function(data){
            //console.log(data);

            parse_and_append_results(data);
            //shoot up the results
        });
}